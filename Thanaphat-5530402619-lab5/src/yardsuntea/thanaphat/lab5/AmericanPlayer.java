package yardsuntea.thanaphat.lab5;

public class AmericanPlayer extends Player {
	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 */

	private String facebookAcct;
	
	public AmericanPlayer(String name, int Day, int Month, int Year, double height, double weight) {
		super(name, Day, Month, Year, height, weight);
	}
	
	public AmericanPlayer(String name, int Day, int Month, int Year,
			double height, double weight, String facebookAcct) {
		super(name, Day, Month, Year, height, weight);
		this.facebookAcct = facebookAcct;
	}

	public void setfacebookAcct(String facebookacct){
		this.facebookAcct = facebookacct;	// For keep String facebookAcct from class StarPlayer.
	}
	
	public String getfacebookAcct() {
		return facebookAcct;	// For return facebookAcct.
	}
}



package yardsuntea.thanaphat.lab5;

public class Player extends yardsuntea.thanaphat.lab4.Player {
	
	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 */
	
	String language = "English";

	public Player(String name, int Day, int Month, int Year, double height,
			double weight) {
		super(name, Day, Month, Year, height, weight);
	} 
	
	public Player(String name, int Day, int Month, int Year, double height,
			double weight, String language) {
		super(name, Day, Month, Year, height, weight);
	}
	
	public void speak(){
		System.out.println("Speak " + language); // Show "Speak English".
		
	}


}

package yardsuntea.thanaphat.lab5;

public class ThaiPlayer extends Player{
	
	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 */
	
	private String award;

	public ThaiPlayer(String name, int Day, int Month, int Year, 
			double height,double weight) {
		super(name, Day, Month, Year, height, weight);
	}
	
	public ThaiPlayer(String name, int Day, int Month, int Year, double height,
			double weight, String award) {
		super(name, Day, Month, Year, height, weight);
		this.award = award;
	}

	public void setAward(String award){
		this.award = award; // For keep String award from class StarPlayer.
	}
	
	public String getAward(){
		return award;	// For return award.
	}
	
	public void speak(){
		System.out.println("�ٴ������"); // For show "�ٴ������".
	}
}

package yardsuntea.thanaphat.lab5;

public class PlayerSpeaking {

	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 */

	static int numPlayer = 3; // Number of player.

	public static void main(String[] args) {
		Player[] players = new Player[numPlayer];
		players[0] = new ThaiPlayer("����� ������", 7, 7, 1985, 57, 169,
				"Best Setters");
		players[1] = new AmericanPlayer("Jeremy Lin", 2, 12, 1988, 91, 191,
				"https://www.facebook.com/jeremylin17");
		players[2] = new ThaiPlayer("�Ѫ�� �Թ�����", 13, 6, 1990, 50, 160);
		for (int i = 0; i < numPlayer; i++) {	// Loop for show the data
			System.out.print(players[i].getName() + " ");
			players[i].speak(); 
		}
	}

}
